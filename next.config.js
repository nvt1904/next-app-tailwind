// const withPlugins = require('next-compose-plugins');
const withBundleAnalyzer = require('@next/bundle-analyzer')({
  enabled: process.env.ANALYZE === 'true',
})
const { i18n } = require('./next-i18next.config');
const nextBuildId = require('next-build-id');

const configNext = {
  reactStrictMode: true,
  i18n,
  pageExtensions: ['page.ts', 'page.tsx'],
  images: {
    domains: String(process.env.IMAGE_DOMAINS).split(',')
  },
  async rewrites() {
    return [
      {
        source: '/',
        destination: '/home'
      }
    ];
  },
  generateBuildId: () => nextBuildId({ dir: __dirname }),
};

module.exports = withBundleAnalyzer(configNext);
