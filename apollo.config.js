module.exports = {
  client: {
    url: process.env.NEXT_PUBLIC_GRAPHQL_API_BASE_URL
  }
};
