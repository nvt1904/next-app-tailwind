const defaultLocale = process.env.NEXT_PUBLIC_LOCALE_DEFAULT || 'vi';
const reloadOnPrerender = process.env.NODE_ENV === 'development';
const locales = [defaultLocale, 'en'];

module.exports = {
  i18n: {
    locales,
    defaultLocale,
    reloadOnPrerender
  }
};
