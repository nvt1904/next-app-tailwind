import { i18n } from 'next-i18next';
import { Ref, Resolver } from 'react-hook-form';
import Validator from 'validatorjs';

export const validatorResolver: Resolver = (values, context, { fields, names }) => {
  Validator['useLang'](i18n.language);
  const rules: { [key: string]: string } = {};
  const attributes: { [key: string]: string } = {};
  names.forEach((name) => {
    const ref: Ref = fields[name].ref;
    if (!ref || !ref['getAttribute']) {
      return;
    }
    const fieldRules = ref['getAttribute']('rules');
    attributes[name] = ref['getAttribute']('data-label');
    if (fieldRules) {
      rules[name] = fieldRules;
    }
  });
  const validator = new Validator(values, rules);
  validator.setAttributeNames(attributes);
  validator.fails();
  return { values, errors: validator.errors?.errors || {} };
};
