import { EventSubscribe, subject } from '@base';
import { DataSession } from '@base/hooks/session/reducer';

// only client
export const getSession = async () => {
  return new Promise<DataSession>((resolve) => {
    const interval = setInterval(() => {
      subject.next({ type: 'getSession' });
    }, 100);
    const subscribe = subject.subscribe((x: EventSubscribe) => {
      if (x.type === 'resultSession') {
        clearInterval(interval);
        subscribe.unsubscribe();
        resolve(x.data);
      }
    });
  });
};
