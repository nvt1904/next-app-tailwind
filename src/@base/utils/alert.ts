import { subject } from '@base';
import { isBrowser } from 'utils/helper';

export enum TYPE {
  ERROR = 'error',
  SUCCESS = 'success',
  INFO = 'info'
}

const alert = {
  show: (type: TYPE, message: string) => {
    if (isBrowser()) {
      const data = {
        type: 'alert',
        data: { type, message }
      };
      if (process.browser) {
        subject.next(data);
      } else {
        console.log(data);
      }
    }
  },
  error: (message?: string) => {
    alert.show(TYPE.ERROR, message || 'Error');
  },
  success: (message?: string) => {
    alert.show(TYPE.SUCCESS, message || 'Success');
  },
  info: (message) => {
    alert.show(TYPE.INFO, message);
  }
};

export default alert;
