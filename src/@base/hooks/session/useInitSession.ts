import { EventSubscribe, subject } from '@base';
import { addMinutes } from 'date-fns';
import { useEffect, useRef } from 'react';
import { Subscription } from 'rxjs';
import { StatusSession } from './reducer';
import useSessionClient from './useSessionClient';

export default function useInitSession() {
  const { isInit, isLoaded, data, setStatus, getSession } = useSessionClient();
  const subscribe = useRef<Subscription>();
  const fetchingSession = useRef(false);
  useEffect(() => {
    const handleSubscribeGetSession = async (x: EventSubscribe) => {
      if (x.type === 'getSession') {
        if (new Date(data?.expires) <= addMinutes(new Date(), 1)) {
          if (!fetchingSession.current) {
            fetchingSession.current = true;
            const newSession = await getSession();
            subject.next({ type: 'resultSession', data: newSession });
            fetchingSession.current = false;
            return;
          }
        } else {
          subject.next({ type: 'resultSession', data });
        }
      }
    };

    const unsubscribe = () => {
      if (subscribe.current) {
        subscribe.current.unsubscribe();
      }
    };

    unsubscribe();
    if (isLoaded) {
      subscribe.current = subject.subscribe(handleSubscribeGetSession);
    }

    return unsubscribe;
  }, [data, isLoaded]);

  useEffect(() => {
    if (!isInit) {
      console.log('initSession');
      setStatus(StatusSession.INIT);
      getSession();
    }
  }, []);
}
