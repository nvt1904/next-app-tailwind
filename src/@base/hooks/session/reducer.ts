import { createSlice, PayloadAction } from '@reduxjs/toolkit';

export type DataSession = {
  accessToken?: string;
  expires?: string;
};

export enum StatusSession {
  DEFAULT = 'null',
  INIT = 'init',
  LOADING = 'loading',
  LOADED = 'loaded'
}

export type SessionState = {
  status: StatusSession;
  data: DataSession | null;
};

const initialState: SessionState = {
  status: StatusSession.DEFAULT,
  data: null
};

const session = createSlice({
  name: 'session',
  initialState,
  reducers: {
    changeStatusSession: (state, action: PayloadAction<StatusSession>) => {
      return { ...state, status: action.payload };
    },
    changeDataSession: (state, action: PayloadAction<DataSession>) => {
      return { ...state, data: action.payload };
    }
  }
});

const { reducer, actions } = session;
export const { changeStatusSession, changeDataSession } = actions;
export default reducer;
