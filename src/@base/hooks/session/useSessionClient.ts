import isEqual from 'lodash/isEqual';
import { getSession as getSessionServer } from 'next-auth/react';
import { AppState } from 'pages/store';
import { useDispatch, useSelector } from 'react-redux';
import { changeDataSession, changeStatusSession, DataSession, StatusSession } from './reducer';

export default function useSessionClient() {
  const data = useSelector((state: AppState) => state.session?.data);
  const status = useSelector((state: AppState) => state.session?.status);
  const dispatch = useDispatch();

  const setData = (newData: DataSession) => {
    dispatch(changeDataSession(newData));
  };

  const setStatus = (newStatus: StatusSession) => {
    dispatch(changeStatusSession(newStatus));
  };

  const getSession = async () => {
    setStatus(StatusSession.LOADING);
    const sessionSS = await getSessionServer();
    let newData: DataSession = null;
    if (sessionSS?.token) {
      const { accessToken, expires } = sessionSS?.token || {};
      newData = { accessToken, expires };
    }
    if (!isEqual(data, newData)) {
      setData(newData);
    }
    setStatus(StatusSession.LOADED);
    return newData;
  };

  return {
    status,
    data,
    setData,
    getSession,
    setStatus,
    isInit: status !== StatusSession.DEFAULT,
    isLoading: status === StatusSession.LOADING,
    isLoaded: status === StatusSession.LOADED,
    isLogged: !!data
  };
}
