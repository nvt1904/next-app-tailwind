import locale from '@base/hooks/locale/reducer';
import nprogress from '@base/hooks/nprogress/reducer';
import ui from '@base/hooks/ui/reducer';
import session from '@base/hooks/session/reducer';

const baseReducer = {
  session,
  locale,
  nprogress,
  ui
};

export default baseReducer;
