import localStorageHelper, { KeyStorage } from '@base/utils/localStorage';
import { useRouter } from 'next/router';
import { useEffect } from 'react';
import useLocale from './useLocale';

function useInitLocale() {
  const router = useRouter();
  const { locale } = useLocale();
  useEffect(() => {
    if (locale.key !== router.locale) {
      localStorageHelper.setObject(KeyStorage.LOCALE, locale);
      router.replace(router.asPath, router.asPath, { locale: locale.key });
    }
  }, [locale]);
}

export default useInitLocale;
