import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import localStorageHelper, { KeyStorage } from '@base/utils/localStorage';

export type Locale = {
  key: string;
  name: string;
};

export const locales = {
  vi: { key: 'vi', name: 'Việt Nam' },
  en: { key: 'en', name: 'English' }
};

export type LocaleKey = keyof typeof locales;

const defaultLocaleKey = process.env.NEXT_PUBLIC_LOCALE_DEFAULT || 'vi';

const initialState: Locale = localStorageHelper.getObject(
  KeyStorage.LOCALE,
  locales[defaultLocaleKey]
) as Locale;

const locale = createSlice({
  name: 'locale',
  initialState: initialState,
  reducers: {
    changeLocale: (state: Locale, action: PayloadAction<Locale>) => {
      Object.assign(state, action.payload);
    }
  }
});

const { reducer, actions } = locale;
export const { changeLocale } = actions;
export default reducer;
