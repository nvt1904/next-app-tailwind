import { configResponsive, useResponsive } from 'ahooks';
import { useEffect, useState } from 'react';
import { isBrowser } from 'utils/helper';

const config = {
  xs: 0,
  sm: 640,
  md: 768,
  lg: 1024,
  xl: 1280,
  '2xl': 1536
};

configResponsive(config);

export type Screen = {
  sizes: Record<keyof typeof config, boolean>;
  width: number;
  height: number;
};

function useScreen() {
  const sizes = useResponsive();
  const [width, setWidth] = useState(0);
  const [height, setHeight] = useState(0);

  useEffect(() => {
    if (isBrowser()) {
      const handleResize = () => {
        const { innerWidth: width, innerHeight: height } = window;
        setWidth(width);
        setHeight(height);
      };
      handleResize();
      window.addEventListener('resize', handleResize);
      return () => window.removeEventListener('resize', handleResize);
    }
  }, []);
  return { sizes, width, height };
}

export default useScreen;
