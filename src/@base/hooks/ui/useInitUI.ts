import localStorageHelper, { KeyStorage } from '@base/utils/localStorage';
import { configResponsive, useResponsive, useToggle } from 'ahooks';
import { useCallback, useEffect, useRef } from 'react';
import { isBrowser } from 'utils/helper';
import useUI from './useUI';

const config = {
  xs: 0,
  sm: 640,
  md: 768,
  lg: 1024,
  xl: 1280,
  '2xl': 1536
};

configResponsive(config);

function useInitUI() {
  const { ui, setUI } = useUI();
  const prefersColorSchemeRef = useRef<MediaQueryList>();
  const [state, { toggle }] = useToggle(false);
  const responsive = useResponsive();

  const onResize = useCallback(() => {
    if (window) {
      const { innerWidth: width, innerHeight: height } = window;
      setUI({ screen: { width, height, responsive } });
      const root = document.documentElement;
      // First we get the viewport height and we multiple it by 1% to get a value for a vh unit
      const vh = window.innerHeight * 0.01;
      // Then we set the value in the --vh custom property to the root of the document
      root.style.setProperty('--height-screen', `calc(${vh}px * 100)`);
    }
  }, [responsive]);

  const changeThemeByPrefersColorScheme = () => {
    if (isBrowser() && ui.theme === 'auto' && prefersColorSchemeRef.current) {
      const dark = prefersColorSchemeRef.current.matches;
      console.log(prefersColorSchemeRef.current.matches);
      document.documentElement.className = dark ? 'dark' : 'light';
    }
  };

  useEffect(() => {
    changeThemeByPrefersColorScheme();
  }, [state]);

  useEffect(() => {
    if (isBrowser() && ui.theme) {
      localStorageHelper.setObject(KeyStorage.UI, ui);
    }
  }, [ui]);

  useEffect(() => {
    if (isBrowser() && ui.theme) {
      const root = document.documentElement;
      Object.keys(ui.color).forEach((property) => {
        root.style.setProperty(`--color-${property}`, ui.color[property]);
      });
    }
  }, [ui.color]);

  useEffect(() => {
    if (isBrowser() && ui.theme) {
      if (ui.theme === 'auto') {
        if (!prefersColorSchemeRef.current) {
          prefersColorSchemeRef.current = window.matchMedia('(prefers-color-scheme: dark)');
        }
        prefersColorSchemeRef.current.onchange = toggle;
        toggle();
      } else {
        document.documentElement.className = ui.theme || 'light';
      }
    }
  }, [ui.theme]);

  useEffect(() => {
    onResize();
    // We listen to the resize event
    window && window.addEventListener('resize', onResize);
    return () => {
      if (isBrowser()) {
        window.removeEventListener('resize', onResize);
      }
      if (prefersColorSchemeRef.current) {
        prefersColorSchemeRef.current.onchange = null;
      }
    };
  }, [responsive]);
}

export default useInitUI;
