import { AppState } from 'pages/store';
import { useDispatch, useSelector } from 'react-redux';
import { changeUI } from './reducer';

export default function useUI() {
  const ui = useSelector((state: AppState) => state.ui);
  const dispatch = useDispatch();
  const setUI = (state) => {
    const action = changeUI({ ...state });
    dispatch(action);
  };
  return { ui, setUI };
}
