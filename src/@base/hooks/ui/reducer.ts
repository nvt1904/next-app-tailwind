import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import localStorageHelper, { KeyStorage } from '@base/utils/localStorage';
import merge from 'lodash/merge';
import { configResponsive } from 'ahooks';

const config = {
  xs: 0,
  sm: 640,
  md: 768,
  lg: 1024,
  xl: 1280,
  '2xl': 1536
};

configResponsive(config);

type ColorTheme = {
  primary: string;
};

export type Screen = {
  sizes: Record<keyof typeof config, boolean>;
  width: number;
  height: number;
};

type UIState = {
  theme: 'light' | 'dark' | 'auto';
  color: ColorTheme;
  screen: Screen;
};

const initialState: UIState = localStorageHelper.getObject(KeyStorage.UI, {
  theme: 'auto',
  color: {
    primary: '#0d6efd'
  }
}) as UIState;

const page = createSlice({
  name: 'ui',
  initialState: initialState,
  reducers: {
    changeUI: (state: UIState, action: PayloadAction<UIState>) => {
      state = merge(state, action.payload);
    }
  }
});

const { reducer, actions } = page;
export const { changeUI } = actions;
export default reducer;
