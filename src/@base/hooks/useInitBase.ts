import useInitAlert from './alert/useInitAlert';
import useInitLocale from './locale/useInitLocale';
import useInitNProgress from './nprogress/useInitNProgress';
import useInitSession from './session/useInitSession';
import useInitUI from './ui/useInitUI';

export default function useInitBase() {
  useInitSession();
  useInitNProgress();
  useInitUI();
  useInitLocale();
  useInitAlert();
}
