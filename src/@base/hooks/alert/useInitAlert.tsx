import { EventSubscribe, subject } from '@base';
import { useEffect } from 'react';
import { AlertCustomOptions, AlertType, useAlert } from 'react-alert';

const Message = ({ message }) => <>{message}</>;

export default function useInitAlert() {
  const alert = useAlert();
  useEffect(() => {
    const subscribe = subject.subscribe((event: EventSubscribe) => {
      const { type, data } = event;
      if (type === 'alert') {
        const options: AlertCustomOptions & { title?: string } = {
          type: data.type as AlertType,
          title: String(data.title)
        };
        alert.show(<Message message={data.message} />, options);
      }
    });
    return () => subscribe.unsubscribe();
  }, []);
}
