import Alert, { options } from '@base/components/alert';
import useInitBase from '@base/hooks/useInitBase';
import dynamic from 'next/dynamic';
import Head from 'next/head';
import React from 'react';
import { Provider as AlertProvider } from 'react-alert';
import { Subject } from 'rxjs';

const ChangeTheme = dynamic(() => import('@base/components/common/ChangeTheme'), {
  ssr: false
});

export type EventSubscribe = { type: string; data: Record<string, unknown> };
export const subject = new Subject();

function BaseWarperInit() {
  useInitBase();
  return (
    <>
      <Head>
        <link rel="shortcut icon" href="/svg/logo.svg" />
        <meta name="viewport" content="width=device-width,initial-scale=1" />
      </Head>
      <div className="fixed bottom-5 left-5 z-50 rounded-full shadow-lg">
        <ChangeTheme />
      </div>
    </>
  );
}

function BaseInit() {
  return (
    <AlertProvider template={Alert} {...options}>
      <BaseWarperInit />
    </AlertProvider>
  );
}

export default React.memo(BaseInit);
