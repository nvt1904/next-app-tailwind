import Head from 'next/head';
import React from 'react';

export type SEOMetaProps = {
  title?: string;
  titleSuffix?: string;
  description?: string;
  keywords?: string;
  robots?: string;
  googlebot?: string;
};

export default function SEOMeta(props: SEOMetaProps) {
  const { titleSuffix, description, keywords, robots, googlebot } = props;
  const title = `${props.title || process.env.NEXT_PUBLIC_APP_NAME}${
    titleSuffix ? ` - ${titleSuffix}` : ''
  }`;
  return (
    <Head>
      <title>{title}</title>
      <meta name="description" content={description || title} />
      <meta name="keywords" content={keywords} />
      <meta name="robots" content={robots || 'index, follow'} />
      {googlebot ? <meta name="googlebot" content={googlebot} /> : null}
    </Head>
  );
}
