import {
  CheckCircleIcon,
  ExclamationCircleIcon,
  InformationCircleIcon,
  XIcon
} from '@heroicons/react/outline';
import React from 'react';
import { positions, transitions } from 'react-alert';

export const options = {
  position: positions.TOP_CENTER,
  timeout: 5000,
  offset: '10px',
  transition: transitions.FADE,
  containerStyle: {
    zIndex: 10000
  }
};

export default function Alert({ message, options, close }) {
  return (
    <div
      style={{ zIndex: 1000, pointerEvents: 'auto' }}
      className="block-component m-4 mb-0 flex flex-nowrap items-center justify-between gap-2 p-2"
    >
      <div className="p-0.5">
        {options.type === 'info' && <InformationCircleIcon className="h-4 w-4" />}
        {options.type === 'success' && <CheckCircleIcon className="h-4 w-4 text-success" />}
        {options.type === 'error' && <ExclamationCircleIcon className="h-4 w-4 text-danger" />}
      </div>
      <div className="flex">
        <span className="text-sm">{message}</span>
      </div>
      <div className="flex">
        <button
          onClick={close}
          className="h-5 w-5 rounded-full border-none p-0.5 hover:bg-light-secondary focus:ring-0 dark:hover:bg-dark-secondary"
        >
          <XIcon className="h-4 w-4" />
        </button>
      </div>
    </div>
  );
}
