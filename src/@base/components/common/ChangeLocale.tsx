import { LocaleKey } from '@base/hooks/locale/reducer';
import useLocale from '@base/hooks/locale/useLocale';

function ChangeLocale() {
  const { locale, langs, setLocale } = useLocale();
  return (
    <select value={locale.key} onChange={(e) => setLocale(e.target.value as LocaleKey)}>
      {langs?.map((item) => (
        <option key={item.value} value={item.value}>
          {item.label}
        </option>
      ))}
    </select>
  );
}

export default ChangeLocale;
