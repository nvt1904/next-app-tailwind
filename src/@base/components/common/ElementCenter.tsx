interface Props extends React.HTMLAttributes<HTMLDivElement> {
  children: string | JSX.Element;
}

const ElementCenter = (props: Props) => {
  const { children, className, ...reset } = props;
  return (
    <div
      className={`flex h-full min-h-inherit items-center justify-center ${className}`}
      {...reset}
    >
      {children}
    </div>
  );
};

export default ElementCenter;
