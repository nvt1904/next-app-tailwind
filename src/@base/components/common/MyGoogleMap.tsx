import { Autocomplete, GoogleMap, LoadScriptNext, Marker } from '@react-google-maps/api';
import React, { useEffect, useRef, useState } from 'react';

const containerStyle = {
  width: '100%',
  height: '400px'
};

type ILatLng = { lat: number; lng: number };

type Props = {
  apiKey: string;
  onChange?: (latLng: ILatLng) => void;
  centerDefault?: ILatLng;
};

function MyGoogleMap(props: Props) {
  const { apiKey, onChange, centerDefault } = props;
  const [pick, setPick] = useState<ILatLng>(centerDefault || { lat: 21.0031177, lng: 105.8201408 });

  const autocompleteRef = useRef<any>();

  const onLoad = (autocomplete) => {
    autocompleteRef.current = autocomplete;
  };

  const onPlaceChanged = () => {
    if (autocompleteRef.current) {
      const place = autocompleteRef.current?.getPlace();
      if (place.geometry?.location) {
        setPick({ lat: place.geometry.location.lat(), lng: place.geometry.location.lng() });
      }
    }
  };

  useEffect(() => {
    if (onChange) {
      onChange(pick);
    }
  }, [pick]);

  return (
    <LoadScriptNext
      libraries={['places']}
      googleMapsApiKey={apiKey || 'AIzaSyAzT63S9denkIRaQ4-FrtzNAiB7haolovg'}
    >
      <GoogleMap
        mapContainerClassName="google-map"
        mapContainerStyle={containerStyle}
        options={{ disableDefaultUI: true }}
        center={centerDefault || { lat: 21.0031177, lng: 105.8201408 }}
        zoom={12}
      >
        <Autocomplete onLoad={onLoad} onPlaceChanged={onPlaceChanged} className="map-autocomplete">
          <input type="text" placeholder="Input search" />
        </Autocomplete>
        <Marker
          animation={2}
          onDragEnd={(e) => setPick({ lat: e.latLng.lat(), lng: e.latLng.lng() })}
          draggable={true}
          visible={true}
          position={pick}
        />
        <></>
      </GoogleMap>
    </LoadScriptNext>
  );
}

export default React.memo(MyGoogleMap);
