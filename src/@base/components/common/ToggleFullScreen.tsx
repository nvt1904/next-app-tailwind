import { useFullscreen } from 'ahooks';
import React from 'react';

interface Props extends React.ButtonHTMLAttributes<HTMLButtonElement> {
  targetId?: string;
}

function ToggleFullScreen(props: Props) {
  const { targetId, ...reset } = props;
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const [isFullscreen, { toggleFullscreen }] = useFullscreen(() =>
    targetId ? window.document.getElementById(targetId) : window.document.body
  );

  return (
    <button onClick={() => toggleFullscreen()} {...reset}>
      Full screen
    </button>
  );
}

export default ToggleFullScreen;
