import React from 'react';
import ReactMarkdown from 'react-markdown';
import { ReactMarkdownOptions } from 'react-markdown/lib/react-markdown';
import emoji from 'remark-emoji';
import gfm from 'remark-gfm';

export default function MarkdownView(props: ReactMarkdownOptions) {
  return (
    <ReactMarkdown
      remarkPlugins={[
        gfm,
        [
          emoji,
          {
            padSpaceAfter: false, // defaults to false
            emoticon: true // defaults to false
          }
        ]
      ]}
      {...props}
    />
  );
}
