import classNames from 'classnames';
import useUI from '@base/hooks/ui/useUI';
import ColorPicker from './ColorPicker';
import { ColorResult } from 'react-color';
import { useState } from 'react';

export default function ChangeTheme() {
  const {
    ui: { theme, color },
    setUI
  } = useUI();
  const [showColorPicker, setShowColorPicker] = useState(false);

  const toggleTheme = () => {
    switch (theme) {
      case 'light':
        setUI({ theme: 'dark' });
        return;
      case 'dark':
        setUI({ theme: 'auto' });
        return;
      default:
        setUI({ theme: 'light' });
    }
  };

  return (
    <div
      onMouseLeave={() => setShowColorPicker(false)}
      onMouseOver={() => setShowColorPicker(true)}
    >
      <div
        className={classNames('block-component absolute bottom-full left-0 mx-0.5 mb-1.5 p-2', {
          hidden: !showColorPicker
        })}
      >
        <ColorPicker
          onChange={(color: ColorResult) => {
            setShowColorPicker(false);
            setUI({ color: { primary: color.hex } });
          }}
          color={color && color.primary}
        />
      </div>
      <button
        className="h-7 w-7 rounded-full border-none bg-light p-1.5 text-primary dark:bg-dark"
        onClick={toggleTheme}
      >
        <svg
          xmlns="http://www.w3.org/2000/svg"
          className="h-4 w-4"
          fill="none"
          viewBox="0 0 24 24"
          stroke="currentColor"
        >
          <path
            className={classNames({ hidden: theme !== 'dark' })}
            strokeLinecap="round"
            strokeLinejoin="round"
            strokeWidth={2}
            d="M20.354 15.354A9 9 0 018.646 3.646 9.003 9.003 0 0012 21a9.003 9.003 0 008.354-5.646z"
          />
          <path
            className={classNames({ hidden: theme !== 'light' })}
            strokeLinecap="round"
            strokeLinejoin="round"
            strokeWidth={2}
            d="M12 3v1m0 16v1m9-9h-1M4 12H3m15.364 6.364l-.707-.707M6.343 6.343l-.707-.707m12.728 0l-.707.707M6.343 17.657l-.707.707M16 12a4 4 0 11-8 0 4 4 0 018 0z"
          />
          <path
            className={classNames({ hidden: theme !== 'auto' })}
            strokeLinecap="round"
            strokeLinejoin="round"
            strokeWidth="2"
            d="M9.75 17L9 20l-1 1h8l-1-1-.75-3M3 13h18M5 17h14a2 2 0 002-2V5a2 2 0 00-2-2H5a2 2 0 00-2 2v10a2 2 0 002 2z"
          />
        </svg>
      </button>
    </div>
  );
}
