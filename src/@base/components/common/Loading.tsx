import ElementCenter from '@base/components/common/ElementCenter';
import React from 'react';
import { SunIcon } from '@heroicons/react/outline';

type Props = {
  loading?: boolean;
  className?: string;
};

const Loading = (props: Props) => {
  const { loading, className } = props;
  return (
    <>
      {loading !== false && (
        <ElementCenter>
          <SunIcon className={`h-5 w-5 animate-spin ${className}`} />
        </ElementCenter>
      )}
    </>
  );
};

export default Loading;
