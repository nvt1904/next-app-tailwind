import Link from 'next/link';
import ElementCenter from '@base/components/common/ElementCenter';
import React from 'react';
import { useTranslation } from 'react-i18next';

const NotFound = () => {
  const { t } = useTranslation();
  return (
    <ElementCenter>
      <div style={{ textAlign: 'center' }}>
        <h2>404 | Not found</h2>
        <button>
          <Link href="/">{t('backHome')}</Link>
        </button>
      </div>
    </ElementCenter>
  );
};

export default NotFound;
