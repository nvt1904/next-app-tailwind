import React from 'react';
import ImageCustom from '../image/ImageCustom';

type ImageRandomProps = {
  height?: string | number;
  width?: string | number;
  className?: string;
};

export default function ImageRandom(props: ImageRandomProps) {
  const src = `https://picsum.photos/${props?.height || 300}/${props?.width || 300}.webp`;
  return <ImageCustom className={props.className} src={src} />;
}
