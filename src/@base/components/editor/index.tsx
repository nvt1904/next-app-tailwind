import MarkdownView from '@base/components/common/MarkdownView';
import React, { useState } from 'react';

export default function Editor() {
  const [content, setContent] = useState('');
  return (
    <div style={{ padding: '2rem' }}>
      <textarea
        id=""
        cols={30}
        rows={10}
        value={content}
        onChange={(e) => setContent(e.target.value)}
      ></textarea>
      <MarkdownView>{content}</MarkdownView>
    </div>
  );
}
