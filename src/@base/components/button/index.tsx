import Loading from '@base/components/common/Loading';
import React from 'react';

type Props = React.ButtonHTMLAttributes<HTMLButtonElement> & {
  loading?: boolean;
};

export default function Button(props: Props) {
  const { children, loading, disabled, ...reset } = props;
  return (
    <button disabled={loading || disabled || false} {...reset}>
      <div className="flex items-center justify-center">
        <Loading loading={loading || false} className="mr-2" />
        {children || 'Button'}
      </div>
    </button>
  );
}
