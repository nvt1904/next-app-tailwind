import { CheckCircleIcon, XCircleIcon } from '@heroicons/react/outline';
import ElementCenter from '@base/components/common/ElementCenter';
import Modal from '@base/components/modal';
import { useEffect, useState } from 'react';
import Cropper from 'react-easy-crop';
import { Area } from 'react-easy-crop/types';
import alert from '@base/utils/alert';
import { getCroppedImg, readFile } from '@base/utils/image';

export type AvatarProps = {
  onChange?: (value: string) => void;
  maxSize?: number;
  disabled?: boolean;
};

export default function Avatar(props: AvatarProps) {
  const { disabled, onChange, maxSize } = props;
  const [imageRaw, setImageRaw] = useState<string>();
  const [crop, setCrop] = useState({ x: 0, y: 0 });
  const [zoom, setZoom] = useState(1);
  const [cropping, setCropping] = useState(false);
  const [image, setImage] = useState<string>();
  const [croppedAreaPixels, setCroppedAreaPixels] = useState<Area>();

  const onChangeFile = async (e) => {
    if (e.target.files && e.target.files.length > 0) {
      const file = e.target.files[0];
      if (maxSize !== undefined && file.size > maxSize) {
        alert.error(`Max size is ${maxSize}`);
        return;
      }
      const src = await readFile(file);
      setImageRaw(src);
      e.target.value = '';
    }
  };

  const onCropComplete = (croppedArea: Area, croppedAreaPixels: Area) => {
    setCroppedAreaPixels(croppedAreaPixels);
  };

  const getImage = async () => {
    const newImage = await getCroppedImg(imageRaw, croppedAreaPixels);
    setImage(newImage);
    setCropping(false);
  };

  useEffect(() => {
    !disabled && setCropping(!!imageRaw);
  }, [imageRaw]);

  useEffect(() => {
    if (!cropping) {
      setImageRaw(null);
    }
  }, [cropping]);

  useEffect(() => {
    !disabled && onChange && onChange(image);
  }, [image]);

  return (
    <>
      <div className="relative h-28 w-28 overflow-hidden rounded-full">
        <input
          disabled={disabled}
          className="h-full w-full cursor-pointer opacity-0"
          type="file"
          onChange={onChangeFile}
        />
        <div className="pointer-events-none absolute top-0 flex h-full w-full cursor-pointer items-center justify-center overflow-hidden">
          {image ? (
            // eslint-disable-next-line @next/next/no-img-element
            <img className="h-full w-full cursor-pointer" src={image} alt="Avatar" />
          ) : (
            'Avatar'
          )}
        </div>
      </div>

      <Modal show={cropping} overlayClose={false} showBtnClose={false}>
        <ElementCenter className="relative h-80 overflow-hidden rounded-lg">
          <>
            {imageRaw ? (
              <Cropper
                image={imageRaw}
                crop={crop}
                zoom={zoom}
                aspect={3 / 3}
                onCropChange={setCrop}
                onCropComplete={onCropComplete}
                onZoomChange={setZoom}
              />
            ) : null}
            <div className="absolute top-2">
              <div className="flex gap-4">
                <XCircleIcon
                  onClick={() => setCropping(false)}
                  className="h-6 w-6 cursor-pointer rounded-full text-light shadow-lg"
                />
                <CheckCircleIcon
                  onClick={getImage}
                  className="h-6 w-6 cursor-pointer rounded-full text-light shadow-lg"
                />
              </div>
            </div>
          </>
        </ElementCenter>
      </Modal>
    </>
  );
}
