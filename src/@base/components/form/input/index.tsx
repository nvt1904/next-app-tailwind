import classNames from 'classnames';
import React, { LegacyRef } from 'react';

type InputProps = React.DetailedHTMLProps<
  React.InputHTMLAttributes<HTMLInputElement>,
  HTMLInputElement
> & {
  label?: string;
  rules?: string;
  error?: string;
};

// eslint-disable-next-line react/display-name
const Input = React.forwardRef((props: InputProps, ref: LegacyRef<HTMLInputElement>) => {
  const { label, error, ...reset } = props;
  return (
    <div className="mb-2">
      <label className="opacity text-sm">{label}</label>
      <input ref={ref} type="text" {...reset} data-label={label} />
      <p
        className={classNames('text-sm text-danger', {
          invisible: !error
        })}
      >
        {error || 'succeed'}
      </p>
    </div>
  );
});

export default Input;
