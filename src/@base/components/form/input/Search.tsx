import React from 'react';
import { useTranslation } from 'react-i18next';
import { useDebounceFn } from 'ahooks';
import Input from '.';

interface Props extends React.InputHTMLAttributes<HTMLInputElement> {
  loading?: boolean;
  onSearch: (value: string) => void;
  wait?: number;
}

export default function Search(props: Props) {
  const { loading, onSearch, wait, ...reset } = props;
  const { t } = useTranslation();
  const { run } = useDebounceFn(
    (value: string) => {
      onSearch(value);
    },
    {
      wait: (wait ? (wait > 0 ? wait : 0) : 0) || 500
    }
  );
  return (
    <Input
      readOnly={loading}
      placeholder={t('inputSearch')}
      onChange={(e) => run(e.target.value)}
      {...reset}
    />
  );
}
