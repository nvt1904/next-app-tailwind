import classNames from 'classnames';
import React, { LegacyRef } from 'react';

type Option = {
  value: string | number;
  label?: string;
};

type SelectProps = React.DetailedHTMLProps<
  React.SelectHTMLAttributes<HTMLSelectElement>,
  HTMLSelectElement
> & {
  label?: string;
  error?: string;
  rules?: string;
  options?: Option[];
};

// eslint-disable-next-line react/display-name
const Select = React.forwardRef((props: SelectProps, ref: LegacyRef<HTMLSelectElement>) => {
  const { label, error, options = [], ...reset } = props;
  return (
    <div className="mb-2">
      <label className="opacity text-sm">{label}</label>
      <select className="pl-0.5" ref={ref} {...reset} data-label={label}>
        {options.map((option) => {
          return (
            <option key={option.value} value={option.value}>
              {option.label || option.value}
            </option>
          );
        })}
      </select>
      <p
        className={classNames('text-sm text-danger', {
          invisible: !error
        })}
      >
        {error || 'succeed'}
      </p>
    </div>
  );
});

export default Select;
