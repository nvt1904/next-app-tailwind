import Image, { ImageProps } from 'next/image';
import React from 'react';

type Props = {
  className?: string;
  src: string;
  alt?: string;
  priority?: boolean;
  loading?: ImageProps['loading'];
};

export default function ImageCustom(props: Props) {
  const { className, ...reset } = props;
  return (
    <Image
      className={`image-custom ${className || ''}`}
      layout="fill"
      objectFit="cover"
      loading="lazy"
      placeholder="blur"
      blurDataURL="/svg/loading-image.svg"
      alt={props.src}
      {...reset}
    />
  );
}
