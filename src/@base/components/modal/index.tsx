import { Dialog, Transition } from '@headlessui/react';
import { XIcon } from '@heroicons/react/outline';
import classNames from 'classnames';
import React, { Fragment } from 'react';

export type ModalProps = {
  show: boolean;
  title?: JSX.Element | string;
  children?: JSX.Element;
  onClose?: () => void;
  overlayClose?: boolean;
  showBtnClose?: boolean;
};

export default function Modal(props: ModalProps) {
  const { show, title, children, overlayClose, onClose, showBtnClose } = props;

  function closeModal() {
    onClose && onClose();
  }

  return (
    <>
      <Transition appear show={show} as={Fragment}>
        <Dialog
          as="div"
          className="fixed inset-0 z-50 overflow-y-auto"
          onClose={() => overlayClose !== false && closeModal()}
        >
          <div className="min-h-screen p-4 text-center">
            <Transition.Child
              as={Fragment}
              enter="ease-out duration-300"
              enterFrom="opacity-0"
              enterTo="opacity-100"
              leave="ease-in duration-300"
              leaveFrom="opacity-100"
              leaveTo="opacity-0"
            >
              <Dialog.Overlay className="fixed inset-0 bg-light-secondary opacity-50 dark:bg-dark-secondary" />
            </Transition.Child>
            <span className="inline-block h-screen align-middle" aria-hidden="true">
              &#8203;
            </span>
            <Transition.Child
              as={Fragment}
              enter="ease-out duration-300"
              enterFrom="opacity-0 scale-95"
              enterTo="opacity-100 scale-100"
              leave="ease-in duration-300"
              leaveFrom="opacity-100 scale-100"
              leaveTo="opacity-0 scale-95"
            >
              <div className="block-component my-8 inline-block w-full max-w-md transform overflow-hidden p-2 text-left align-middle transition-all">
                <div
                  className={classNames('absolute top-2 right-2 z-20', {
                    'z-0 opacity-0': showBtnClose === false
                  })}
                >
                  <button
                    onClick={closeModal}
                    className="h-5 w-5 rounded-full border-none p-0.5 hover:bg-light-secondary focus:ring-0 dark:hover:bg-dark-secondary"
                  >
                    <XIcon className="h-4 w-4" />
                  </button>
                </div>
                <Dialog.Title>{title}</Dialog.Title>
                {children}
              </div>
            </Transition.Child>
          </div>
        </Dialog>
      </Transition>
    </>
  );
}
