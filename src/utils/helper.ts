import _ from 'lodash';

export function trimAll(data: any): any {
  if ([null, undefined, true, false].indexOf(data) >= 0) {
    return data;
  }
  if (typeof data === 'string') {
    return data.trim();
  }
  if (Array.isArray(data)) {
    return data.map((x) => trimAll(x));
  }
  if (typeof data === 'object') {
    const newObject = { ...data };
    Object.keys(newObject).map((k) => {
      newObject[k] = trimAll(newObject[k]);
    });
    return newObject;
  }
  return data;
}

export enum defaultString {
  blank = ''
}

type DataReplace = {
  [key: string]: string | number;
};

export const replaceWithData = (
  str: string,
  data: DataReplace,
  keyStart = '{',
  keyEnd = '}'
): string => {
  for (const [key, value] of Object.entries(data)) {
    str = str.replace(new RegExp(`${keyStart}${key}${keyEnd}`, 'g'), `${value?.toString()}`);
  }
  return str;
};

export const getUrl = (str: string, params: DataReplace): string => {
  return replaceWithData(str, params, '/:', '');
};

export type OptionStrGenerate = {
  length?: number;
  prefix?: string;
  suffix?: string;
  upCase?: boolean;
  lowerCase?: boolean;
};

export const strGenerate = (options?: OptionStrGenerate): string => {
  let result = '';
  const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  const charactersLength = characters.length;
  for (let i = 0; i < (options?.length || 10); i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  if (options?.prefix) {
    result = `${options?.prefix}${result}`;
  }
  if (options?.suffix) {
    result = `${result}${options?.suffix}`;
  }
  if (options?.upCase && !options?.lowerCase) {
    result = result.toUpperCase();
  }
  if (options?.lowerCase && !options?.upCase) {
    result = result.toLowerCase();
  }
  return result;
};

interface ObjectMultiDepth {
  [key: string]:
    | boolean
    | string
    | number
    | ObjectMultiDepth
    | Array<number | string | boolean | ObjectMultiDepth>;
}
interface ObjectOneDepth {
  [key: string]: boolean | string | number | Array<number | string | boolean>;
}

export const objectMultiToOneDepth = (data: ObjectMultiDepth, prefix = '') => {
  const dataRaw = { ...data };
  let newData: ObjectOneDepth = {};
  // eslint-disable-next-line guard-for-in
  for (const key in dataRaw) {
    if (typeof dataRaw[key] === 'object') {
      newData = _.merge(
        newData,
        objectMultiToOneDepth(dataRaw[key] as ObjectMultiDepth, `${prefix}${key}.`)
      );
    } else {
      newData[`${prefix}${key}`] = dataRaw[key] as ObjectOneDepth['key'];
    }
  }
  return newData;
};

export const objectOneToMultiDepth = (data: ObjectOneDepth) => {
  const dataRaw = { ...data };
  let newData: ObjectMultiDepth = {};

  function fnHelper(
    keys: string[],
    value: boolean | string | number | Array<number | string | boolean>
  ) {
    const newObject: ObjectMultiDepth = {};
    if (keys.length > 0) {
      const keyFirst = keys.shift();
      if (keyFirst) {
        if (keys.length > 0) {
          // eslint-disable-next-line @typescript-eslint/no-unused-vars
          newObject[keyFirst] = fnHelper(keys, value);
        } else {
          newObject[keyFirst] = value;
        }
      }
    }
    return newObject;
  }
  // eslint-disable-next-line guard-for-in
  for (const key in dataRaw) {
    const keys = key.split('.');
    newData = { ..._.merge(fnHelper(keys, dataRaw[key]), newData) };
  }
  return newData;
};

export const sortObjectByKey = (data: ObjectMultiDepth) => {
  const dataRaw = { ...data };
  const newData: ObjectMultiDepth = {};
  _(dataRaw)
    .keys()
    .sort()
    .each(function (key) {
      if (typeof dataRaw[key] === 'object') {
        newData[key] = sortObjectByKey(dataRaw[key] as ObjectMultiDepth);
      } else {
        newData[key] = dataRaw[key];
      }
    });
  return newData;
};

export const isBrowser = () => typeof window !== 'undefined';
