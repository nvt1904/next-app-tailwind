export type File = {
  id: string;
  name: string;
  publicId: string;
  format: string;
  type: string;
  bytes: number;
  url: string;
  access: string;
  createdAt: string;
  updatedAt: string;
};

export type User = {
  id: string;
  avatar?: File;
  name: string;
};
