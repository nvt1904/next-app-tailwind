import axiosClient, { ListParams, ListRes } from 'utils/axiosClient';
import { Message } from './message';
import { User } from './type';

export type ListConversationParams = ListParams;

export type Conversation = {
  id: string;
  users: User[];
  message: Message;
  messages: Message[];
  createdAt: string;
  updatedAt: string;
};

export const list = (params: ListConversationParams) => {
  const url = 'conversation';
  return axiosClient.get<ListRes<Conversation>>(url, params, { authorization: true });
};

const conversationApi = {
  list
};

export default conversationApi;
