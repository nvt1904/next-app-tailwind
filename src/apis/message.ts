import axiosClient, { ListParams, ListRes, Res } from 'utils/axiosClient';

export type Message = {
  id: string;
  content: string;
  userId: string;
  conversationId: string;
  createdAt: string;
  updatedAt: string;
  deleted: null;
  files: File[];
};

// list message
export type ListMessageParams = ListParams<{
  conversationId: string;
}>;

const list = (params: ListMessageParams) => {
  const url = 'message';
  return axiosClient.get<ListRes<Message>>(url, params, { authorization: true });
};

// create message
export type CreateMessageData = {
  userId?: string;
  conversationId?: string;
  content?: string;
  files?: string[];
};

export type CreateMessageRes = Res<{
  id: string;
}>;

const create = (data: CreateMessageData) => {
  const url = 'message';
  return axiosClient.post<CreateMessageRes>(url, data, { authorization: true });
};

const messageApi = {
  create,
  list
};

export default messageApi;
