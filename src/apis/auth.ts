import axiosClient, { HeaderConf, Res } from 'utils/axiosClient';

// register
export type RegisterData = {
  name: string;
  email: string;
  password: string;
};

export type RegisterRes = Res<{
  id: string;
}>;

export const register = (data: RegisterData) => {
  const url = 'auth/register';
  return axiosClient.post<RegisterRes>(url, data);
};

// login
export type DataLogin = {
  email: string;
  password: string;
};

export type LoginRes = Res<{
  accessToken: string;
  refreshToken: string;
  expires: string;
}>;

export const login = (data: DataLogin, headerConf?: HeaderConf) => {
  const url = 'auth/login';
  return axiosClient.post<LoginRes>(url, data, headerConf);
};

// refreshToken
export type DataRefreshToken = {
  refreshToken: string;
};

export type RefreshTokenRes = Res<{
  accessToken: string;
  expires: string;
}>;

export const refreshToken = (data: DataRefreshToken) => {
  const url = 'auth/refresh-token';
  return axiosClient.post<RefreshTokenRes>(url, data);
};

// profile
export enum Status {
  ACTIVE = 'active',
  INACTIVE = 'inactive'
}

export type ProfileRes = Res<{
  id: string;
  avatar?: File;
  name: string;
  email: string;
  status: Status;
}>;

export const getProfile = () => {
  const url = 'auth/profile';
  return axiosClient.get<ProfileRes>(url, {}, { authorization: true });
};

// update profile
export type UpdateProfileData = {
  avatar?: string;
  name?: string;
};

export type UploadProfileRes = Res<{
  success: boolean;
}>;

export const updateProfile = (data: UpdateProfileData) => {
  const url = 'auth/profile';
  return axiosClient.put<UploadProfileRes>(url, data, { authorization: true });
};

// forgot password
export type ForgotPasswordData = {
  email: string;
};

export type ForgotPasswordRes = Res<{
  success: boolean;
}>;

export const forgotPassword = (data: ForgotPasswordData) => {
  const url = 'auth/forgot-password';
  return axiosClient.post<ForgotPasswordRes>(url, data);
};

const authApi = {
  register,
  login,
  refreshToken,
  getProfile,
  updateProfile,
  forgotPassword
};

export default authApi;
