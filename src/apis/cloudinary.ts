import { stringifyUrl } from 'query-string';
import axiosClient, { Res } from 'utils/axiosClient';

export type FileCloudinary = {
  asset_id: string;
  public_id: string;
  version: number;
  version_id: string;
  signature: string;
  width: number;
  height: number;
  format: string;
  resource_type: string;
  created_at: string;
  tags: string[];
  bytes: number;
  type: string;
  etag: string;
  placeholder: false;
  url: string;
  secure_url: string;
  access_mode: string;
  original_filename: string;
  api_key: string;
};

// sign url upload temp
export type SignUrlUploadTempRes = Res<{
  public_id: string;
  timestamp: number;
  signature: string;
  api_key: string;
}>;

export const signUrlUploadTemp = () => {
  const url = '/cloudinary/sign-url-upload-temp';
  return axiosClient.get<SignUrlUploadTempRes>(url);
};

// upload temp
export type UploadTempRes = Res<FileCloudinary>;

export const uploadTemp = async (imageBase64: string) => {
  const result = await signUrlUploadTemp();
  if (result.error) {
    return {
      error: result.error,
      message: result.message
    } as UploadTempRes;
  }
  const data = new FormData();
  data.append('file', imageBase64);
  return axiosClient.post<UploadTempRes>(
    stringifyUrl({
      url: `https://api.cloudinary.com/v1_1/${process.env.NEXT_PUBLIC_CLOUDINARY_NAME}/auto/upload`,
      query: result
    }),
    data
  );
};

const cloudinaryApi = {
  uploadTemp
};

export default cloudinaryApi;
