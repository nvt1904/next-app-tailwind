import Button from '@base/components/button';
import AvatarDefault from '@base/components/common/AvatarDefault';
import ElementCenter from '@base/components/common/ElementCenter';
import Loading from '@base/components/common/Loading';
import MarkdownView from '@base/components/common/MarkdownView';
import ImageCustom from '@base/components/image/ImageCustom';
import { ArrowLeftIcon, DotsHorizontalIcon } from '@heroicons/react/outline';
import { Message } from 'apis/message';
import classNames from 'classnames';
import { format } from 'date-fns';
import useProfile from 'hooks/auth/useProfile';
import useListConversation from 'hooks/message/useListConversation';
import useListMessage from 'hooks/message/useListMesage';
import React, { useEffect, useMemo, useRef } from 'react';
import PerfectScrollbar from 'react-perfect-scrollbar';
import InputContent from './InputContent';

export default function ListMessage() {
  const ref = useRef<PerfectScrollbar>();
  const distanceButtonRef = useRef(0);
  const lastMessageRef = useRef<Message>();
  const { conversations, conversationId, changeConversation } = useListConversation();
  const { messages, getListMessage, loadMoreMessage } = useListMessage();
  const { data } = useProfile();

  const receiver = useMemo(() => {
    if (conversationId && data) {
      return conversations[conversationId].users.find((user) => user.id !== data.id);
    }
    return null;
  }, [conversationId]);

  const onLoadMoreMessage = (el: HTMLElement) => {
    if (!messages[conversationId].loading && el.scrollTop <= 100) {
      loadMoreMessage(conversationId);
    }
  };

  const onChangeDistanceButtonRef = (el: HTMLElement) => {
    distanceButtonRef.current = el.scrollHeight - el.offsetHeight - el.scrollTop;
  };

  const scrollBottom = () => {
    if (ref.current) {
      const el = ref.current['_container'];
      el.scrollTo(0, el.scrollHeight - el.offsetHeight);
      distanceButtonRef.current = 0;
    }
  };

  useEffect(() => {
    const fn = async () => {
      if (conversationId && !Object.keys(messages[conversationId]?.items).length) {
        await getListMessage({ conversationId });
      }
      lastMessageRef.current = conversations[conversationId]?.message;
    };
    fn();
  }, [conversationId]);

  useEffect(() => {
    if (distanceButtonRef.current <= 100) {
      scrollBottom();
    } else {
      if (ref.current) {
        const el = ref.current['_container'];
        const newLastMessage = conversations[conversationId]?.message;
        if (lastMessageRef.current) {
          if (newLastMessage?.id !== lastMessageRef.current?.id) {
            distanceButtonRef.current = el.scrollHeight - el.offsetHeight - el.scrollTop;
            el.scrollTo(0, el.scrollHeight - el.offsetHeight - distanceButtonRef.current);
          } else {
            el.scrollTo(0, el.scrollHeight - el.offsetHeight - distanceButtonRef.current);
            distanceButtonRef.current = el.scrollHeight - el.offsetHeight - el.scrollTop;
          }
        }
      }
    }
  }, [messages, messages[conversationId]?.loading]);

  if (!conversationId) {
    return <></>;
  }

  return (
    <div className="flex h-inherit max-h-full flex-col">
      <div className="flex flex-row items-center justify-between p-2 shadow-sm">
        <div className="flex flex-row items-center">
          <Button
            onClick={() => changeConversation(undefined)}
            className="rounded-full p-2 hover:bg-light-secondary dark:hover:bg-dark-secondary"
          >
            <ArrowLeftIcon className="h-4 w-4" />
          </Button>
        </div>
        <div className="flex flex-row items-center">
          <div className="relative mr-2 h-8 w-8">
            {receiver?.avatar ? (
              <ImageCustom className="rounded-full" src={receiver?.avatar.url} />
            ) : (
              <AvatarDefault className="h-8 w-8" />
            )}
          </div>
          <h2>{receiver?.name}</h2>
        </div>
        <div>
          <Button className="rounded-full p-2 hover:bg-light-secondary dark:hover:bg-dark-secondary">
            <DotsHorizontalIcon className="h-4 w-4" />
          </Button>
        </div>
      </div>
      <PerfectScrollbar
        ref={ref}
        onScrollUp={onLoadMoreMessage}
        onScrollY={onChangeDistanceButtonRef}
      >
        <div className="flex h-full min-h-inherit flex-grow flex-col gap-2 p-2">
          {messages[conversationId]?.loading && (
            <ElementCenter>
              <Loading />
            </ElementCenter>
          )}
          {messages[conversationId]?.items?.map((item) => (
            <div
              key={item.id}
              className={classNames('flex flex-row', {
                'justify-end': item.userId !== receiver?.id,
                'justify-start': item.userId === receiver?.id
              })}
            >
              <div
                className={classNames(
                  'max-w-sm rounded-lg border px-2 pt-1 shadow-lg dark:border-dark-secondary',
                  {
                    'bg-primary text-light': item.userId !== receiver?.id
                  }
                )}
              >
                <MarkdownView>{item.content}</MarkdownView>
                <div className="text-right">
                  <span className="text-xs opacity-50">
                    {format(new Date(item.createdAt), 'HH:mm')}
                  </span>
                </div>
              </div>
            </div>
          ))}
        </div>
      </PerfectScrollbar>
      <div className="p-2">
        <InputContent conversationId={conversationId} />
      </div>
    </div>
  );
}
