import AvatarDefault from '@base/components/common/AvatarDefault';
import ElementCenter from '@base/components/common/ElementCenter';
import Loading from '@base/components/common/Loading';
import MarkdownView from '@base/components/common/MarkdownView';
import ImageCustom from '@base/components/image/ImageCustom';
import { Conversation } from 'apis/conversation';
import classNames from 'classnames';
import useProfile from 'hooks/auth/useProfile';
import { changeConversationId } from 'hooks/message/reducer';
import useListConversation from 'hooks/message/useListConversation';
import { useTranslation } from 'next-i18next';
import React, { useEffect } from 'react';
import PerfectScrollbar from 'react-perfect-scrollbar';
import { useDispatch } from 'react-redux';

type Props = {
  conversationId?: string;
};

export default function ListConversation(props: Props) {
  const { t } = useTranslation();
  const { data } = useProfile();
  const dispatch = useDispatch();
  const {
    init,
    conversations,
    conversationId,
    loading,
    getListConversation,
    loadMoreConversation
  } = useListConversation();

  const onLoadMoreMessage = (el: HTMLElement) => {
    if (!loading && el.scrollHeight - (el.scrollTop + el.offsetHeight) <= 100) {
      loadMoreConversation();
    }
  };

  useEffect(() => {
    if (!init) {
      !Object.keys(conversations).length && getListConversation();
    }
  }, []);

  useEffect(() => {
    if (props.conversationId && conversationId !== props.conversationId) {
      dispatch(changeConversationId(props.conversationId));
    }
  }, [props.conversationId]);

  return (
    <div className="h-max-full flex flex-col">
      <div className="p-2">
        <input placeholder={t('search')} />
      </div>
      <PerfectScrollbar onScrollDown={onLoadMoreMessage}>
        <div className="flex flex-grow flex-col gap-2 px-2">
          {Object.values(conversations)?.map((item: Conversation) => {
            const receiver = item?.users?.find((user) => user.id !== data?.id);
            return (
              <div
                key={item.id}
                onClick={() => dispatch(changeConversationId(item.id))}
                className={classNames(
                  'cursor-pointer rounded-lg px-2 py-1 hover:bg-primary hover:text-light',
                  {
                    'bg-primary text-light': item.id === conversationId
                  }
                )}
              >
                <div className="flex flex-row items-center">
                  <div className="relative mr-2 h-8 w-8">
                    {receiver?.avatar ? (
                      <ImageCustom className="rounded-full" src={receiver?.avatar.url} />
                    ) : (
                      <AvatarDefault className="h-8 w-8" />
                    )}
                  </div>
                  <div className="overflow-hidden">
                    <h2 className="line-clamp-1">{receiver?.name}</h2>
                    <MarkdownView className="text-xs line-clamp-1">
                      {item.message?.content}
                    </MarkdownView>
                  </div>
                </div>
              </div>
            );
          })}
          {loading && (
            <ElementCenter>
              <Loading />
            </ElementCenter>
          )}
        </div>
      </PerfectScrollbar>
    </div>
  );
}
