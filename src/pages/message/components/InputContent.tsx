import { PaperAirplaneIcon } from '@heroicons/react/outline';
import Button from '@base/components/button';
import useCreateMessage from 'hooks/message/useCreateMessage';
import React, { useRef, useState } from 'react';

export default function InputContent({ conversationId }) {
  const ref = useRef<HTMLTextAreaElement>();
  const [content, setContent] = useState('');
  const { pending, createMessage } = useCreateMessage();

  const onKeyPressContent = async (e) => {
    if (!e.shiftKey && e.key === 'Enter') {
      submitMessage();
    }
  };

  const submitMessage = async () => {
    if (content.trim()) {
      const result = await createMessage({
        conversationId,
        content
      });
      if (result && result.id) {
        setContent('');
      }
    } else {
      setContent('');
    }
    ref.current?.focus();
  };

  return (
    <div className="flex items-center gap-2">
      <textarea
        ref={ref}
        rows={1.5}
        disabled={pending}
        value={content}
        onChange={(e) => setContent(e.target.value)}
        onKeyPress={onKeyPressContent}
      />
      <Button
        className="rounded-full p-2 hover:bg-primary hover:text-light"
        onClick={submitMessage}
      >
        <PaperAirplaneIcon className="h-5 w-5 rotate-45" />
      </Button>
    </div>
  );
}
