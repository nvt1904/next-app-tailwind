import classNames from 'classnames';
import useListConversation from 'hooks/message/useListConversation';
import { withSSAuth } from 'middleware/withSSAuth';
import { withTranslations } from 'middleware/withSSTranslations';
import Master from 'pages/components/layout';
import React from 'react';
import ListConversation from './components/ListConversation';
import ListMessage from './components/ListMessage';

export default function MessagesIndex() {
  const { conversationId } = useListConversation();

  return (
    <div className="h-mh-content-page">
      <div className="relative grid h-inherit grid-flow-col grid-cols-12">
        <div
          className={classNames(
            'z-10 col-span-12 h-inherit border-r dark:border-dark-secondary sm:col-span-3 sm:block lg:col-span-2',
            {
              hidden: conversationId
            }
          )}
        >
          <ListConversation />
        </div>
        <div className="col-span-12 h-inherit sm:col-span-9 lg:col-span-10">
          <ListMessage />
        </div>
      </div>
    </div>
  );
}

MessagesIndex.Layout = Master;

export const getServerSideProps = withSSAuth(withTranslations());
