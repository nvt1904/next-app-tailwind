import BaseInit from '@base';
import '@base/styles/index.scss';
import useInitProfile from 'hooks/auth/useInitProfile';
import useInitSocketIO from 'hooks/socket-io/useInitSocketIO';
import { appWithTranslation } from 'next-i18next';
import type { AppProps } from 'next/app';
import store from 'pages/store';
import React from 'react';
import { Provider } from 'react-redux';
import 'styles/index.scss';

// export function reportWebVitals(metric) {
//   console.log(metric)
// }

const AppInit = () => {
  useInitProfile();
  useInitSocketIO();
  return <></>;
};

function App({ Component, pageProps }: AppProps) {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const Layout = (Component as any).Layout || (({ children }) => <>{children}</>);
  return (
    <Provider store={store}>
      <BaseInit />
      <AppInit />
      <Layout>
        <Component {...pageProps} />
      </Layout>
    </Provider>
  );
}

export default appWithTranslation(App);
