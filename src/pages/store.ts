import { configureStore, ConfigureStoreOptions } from '@reduxjs/toolkit';
import baseReducer from '@base/hooks/baseReducer';
import profile from 'hooks/auth/reducer';
import message from 'hooks/message/reducer';
import { createStateSyncMiddleware, initMessageListener } from 'redux-state-sync';
import { isBrowser } from 'utils/helper';

const rootReducer = {
  ...baseReducer,
  profile,
  message
};

const options: ConfigureStoreOptions = {
  reducer: rootReducer
};

options.middleware = []
if (isBrowser()) {
  options.middleware = options.middleware.concat([
    createStateSyncMiddleware({
      whitelist: [
        'locale/changeLocale',
        'ui/changeUI',
        'session/changeStatusSession',
        'session/changeDataSession',
        'profile/changeStatusProfile',
        'profile/changeDataProfile',
        'message/pushMessage',
        'message/pushConversation'
      ]
    })
  ]);
}

const store = configureStore(options);

if (isBrowser()) {
  initMessageListener(store);
}

export type AppState = ReturnType<typeof store.getState>;
export default store;
