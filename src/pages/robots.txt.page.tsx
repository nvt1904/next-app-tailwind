export const getRobotsContent = () => {
  switch (process.env.ENVIRONMENT) {
    case 'prod':
      return `User-agent: *
Allow: /
Sitemap: ${process.env.BASE_URL}/sitemap.xml
Disallow: /api/*`;
    default:
      return `User-agent: *
Disallow: / `;
  }
};

const Robots = () => null;

export default Robots;

export async function getServerSideProps({ res }) {
  const result = getRobotsContent();

  res.setHeader('content-type', 'text/plain;charset=UTF-8');
  res.setHeader('Cache-Control', 'public,max-age=30');
  res.write(result);
  res.end();

  return {
    props: {}
  };
}
