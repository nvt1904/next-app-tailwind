import Button from '@base/components/button';
import Input from '@base/components/form/input';
import useSessionClient from '@base/hooks/session/useSessionClient';
import { validatorResolver } from '@base/utils/validator';
import useForgotPassword from 'hooks/auth/useForgotPassword';
import head from 'lodash/head';
import Link from 'next/link';
import { useRouter } from 'next/router';
import FormCard from 'pages/auth/components/FormCard';
import { AppState } from 'pages/store';
import { useEffect } from 'react';
import { useForm } from 'react-hook-form';
import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';

const Form = () => {
  const { t } = useTranslation();
  const router = useRouter();
  const { isLoaded, data } = useSessionClient();
  const {
    register,
    handleSubmit,
    formState: { errors }
  } = useForm({
    resolver: validatorResolver
  });
  const { pending, forgotPassword } = useForgotPassword();
  const { loading } = useSelector((state: AppState) => state.nprogress);

  useEffect(() => {
    if (isLoaded && data) {
      router.replace(String(router.query?.callbackUrl || '/'));
    }
  }, [isLoaded, data]);

  const onSubmit = async (values) => {
    forgotPassword({
      email: values.email
    });
  };

  return (
    <FormCard title={t('form:forgotPassword.title')}>
      <form onSubmit={handleSubmit(onSubmit)}>
        <div className="flex flex-col">
          <div>
            <Input
              label={t('form:forgotPassword.label.email')}
              rules="required"
              error={head(errors.email)}
              {...register('email')}
            />
          </div>

          <div className="p-2 text-center">
            <Button className="primary" loading={pending} disabled={loading || pending}>
              {t('form:forgotPassword.button.submit')}
            </Button>
          </div>

          <div className="text-center text-sm">
            <span>
              {t('form:forgotPassword.suggest.login')}
              <Link href="/auth/login">
                <a className="ml-2">{t('form:forgotPassword.button.login')}</a>
              </Link>
            </span>
          </div>
        </div>
      </form>
    </FormCard>
  );
};

export default Form;
