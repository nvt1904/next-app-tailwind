import ElementCenter from '@base/components/common/ElementCenter';
import SEOMeta from '@base/components/head/SEOMeta';
import { withTranslations } from 'middleware/withSSTranslations';
import { withSSUnAuth } from 'middleware/withSSUnAuth';
import { useTranslation } from 'next-i18next';
import AuthLayout from 'pages/auth/components/layout';
import Form from './components/Form';

const RegisterPage = () => {
  const { t } = useTranslation();
  return (
    <>
      <SEOMeta titleSuffix={t('register')} />
      <ElementCenter className="h-screen">
        <Form />
      </ElementCenter>
    </>
  );
};

RegisterPage.Layout = AuthLayout;

export const getServerSideProps = withSSUnAuth(withTranslations());

export default RegisterPage;
