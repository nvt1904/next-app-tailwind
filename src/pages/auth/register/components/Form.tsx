import Button from '@base/components/button';
import Input from '@base/components/form/input';
import { validatorResolver } from '@base/utils/validator';
import useRegister from 'hooks/auth/useRegister';
import head from 'lodash/head';
import useSessionClient from '@base/hooks/session/useSessionClient';
import Link from 'next/link';
import { useRouter } from 'next/router';
import FormCard from 'pages/auth/components/FormCard';
import { AppState } from 'pages/store';
import React, { useEffect } from 'react';
import { useForm } from 'react-hook-form';
import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';

const Form = () => {
  const { t } = useTranslation();
  const router = useRouter();
  const { isLoaded, data } = useSessionClient();
  const {
    register,
    handleSubmit,
    formState: { errors }
  } = useForm({
    resolver: validatorResolver
  });
  const { pending, register: authRegister } = useRegister();
  const { loading } = useSelector((state: AppState) => state.nprogress);

  useEffect(() => {
    if (isLoaded && data) {
      router.replace(String(router.query?.callbackUrl || '/'));
    }
  }, [isLoaded, data]);

  const onSubmit = async (values) => {
    authRegister({
      name: values.name,
      email: values.email,
      password: values.password
    });
  };

  return (
    <FormCard title={t('form:register.title')}>
      <form onSubmit={handleSubmit(onSubmit)}>
        <div className="flex flex-col">
          <div>
            <Input
              label={t('form:register.label.name')}
              rules="required"
              error={head(errors.name)}
              {...register('name')}
            />
          </div>
          <div>
            <Input
              label={t('form:register.label.email')}
              rules="required"
              error={head(errors.email)}
              {...register('email')}
            />
          </div>
          <div>
            <Input
              label={t('form:register.label.password')}
              rules="required|min:8"
              error={head(errors.password)}
              type="password"
              {...register('password')}
            />
          </div>
          <div>
            <Input
              label={t('form:register.label.passwordConfirm')}
              rules="same:password"
              error={head(errors.passwordConfirm)}
              type="password"
              {...register('passwordConfirm')}
            />
          </div>

          <div className="p-2 text-center">
            <Button className="primary" loading={pending} disabled={loading || pending}>
              {t('form:register.button.register')}
            </Button>
          </div>

          <div className="text-center text-sm">
            <span>
              {t('form:register.suggest.login')}
              <Link href="/auth/login">
                <a className="ml-2">{t('form:register.button.login')}</a>
              </Link>
            </span>
          </div>
        </div>
      </form>
    </FormCard>
  );
};

export default Form;
