import ElementCenter from '@base/components/common/ElementCenter';
import Logo from 'pages/components/logo';
import React from 'react';

export default function FormCard({ children, title = '' }) {
  return (
    <div className="block-component p-4">
      <div className="mb h-10">
        <ElementCenter>
          <Logo className="h-10 w-10 text-primary" />
        </ElementCenter>
      </div>
      <h1 className="p-4 text-center text-lg uppercase text-primary">{title}</h1>
      <>{children}</>
    </div>
  );
}
