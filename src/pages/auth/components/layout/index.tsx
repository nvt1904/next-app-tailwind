import OGMeta from '@base/components/head/OGMeta';
import SEOMeta from '@base/components/head/SEOMeta';
import ImageCustom from '@base/components/image/ImageCustom';
import useUI from '@base/hooks/ui/useUI';
import React from 'react';
import PerfectScrollbar from 'react-perfect-scrollbar';

export default function AuthLayout({ children }) {
  const {
    ui: { theme }
  } = useUI();
  return (
    <div className="relative h-screen">
      <SEOMeta />
      <OGMeta />
      <ImageCustom
        className="h-screen"
        src={
          theme === 'dark'
            ? `https://picsum.photos/1902/1080.webp?grayscale`
            : `https://picsum.photos/1902/1080.webp`
        }
        alt="https://picsum.photos/1920/1080.webp"
      />
      <PerfectScrollbar>{children}</PerfectScrollbar>
    </div>
  );
}
