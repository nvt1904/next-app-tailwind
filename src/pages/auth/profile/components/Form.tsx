import Button from '@base/components/button';
import ElementCenter from '@base/components/common/ElementCenter';
import Input from '@base/components/form/input';
import alert from '@base/utils/alert';
import { validatorResolver } from '@base/utils/validator';
import useProfile from 'hooks/auth/useProfile';
import useUpdateProfile from 'hooks/auth/useUpdateProfile';
import useUploadCloudinary from 'hooks/upload/useUploadCloudinary';
import head from 'lodash/head';
import dynamic from 'next/dynamic';
import FormCard from 'pages/auth/components/FormCard';
import React, { useEffect } from 'react';
import { useForm } from 'react-hook-form';
import { useTranslation } from 'react-i18next';

const Avatar = dynamic(() => import('@base/components/form/avatar'), { ssr: false });

const Form = () => {
  const { t } = useTranslation();
  const { data, getProfile } = useProfile();
  const {
    register,
    handleSubmit,
    setValue,
    formState: { errors }
  } = useForm({
    resolver: validatorResolver
  });
  const { pending, updateProfile, result } = useUpdateProfile();
  const { pending: uploading, uploadCloudinary, result: imageAvatar } = useUploadCloudinary();

  useEffect(() => {
    if (result?.error) {
      alert.error(result?.error);
    }
    if (result) {
      alert.success();
      getProfile();
    }
  }, [result]);

  useEffect(() => {
    if (data) {
      Object.keys(data).forEach((key) => {
        setValue(key, data[key]);
      });
    }
  }, [data]);

  const onSubmit = async (values) => {
    updateProfile({
      avatar: imageAvatar?.public_id,
      name: values.name
    });
  };

  return (
    <FormCard title={t('form:profile.title')}>
      <form onSubmit={handleSubmit(onSubmit)}>
        <div className="mb-4 h-28">
          <ElementCenter>
            <Avatar disabled={uploading} onChange={uploadCloudinary} />
          </ElementCenter>
        </div>
        <div className="flex flex-col">
          <div>
            <Input
              readOnly
              label={t('form:profile.label.email')}
              rules="required"
              error={head(errors.email)}
              {...register('email')}
            />
          </div>
          <div>
            <Input
              label={t('form:profile.label.name')}
              rules="required"
              error={head(errors.name)}
              {...register('name')}
            />
          </div>
          <div className="p-2 text-center">
            <Button className="primary" loading={pending} disabled={pending || uploading}>
              {t('form:profile.button.update')}
            </Button>
          </div>
        </div>
      </form>
    </FormCard>
  );
};

export default Form;
