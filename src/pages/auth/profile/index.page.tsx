import ElementCenter from '@base/components/common/ElementCenter';
import SEOMeta from '@base/components/head/SEOMeta';
import { withSSAuth } from 'middleware/withSSAuth';
import { withTranslations } from 'middleware/withSSTranslations';
import { useTranslation } from 'next-i18next';
import AuthLayout from 'pages/auth/components/layout';
import Form from './components/Form';

const ProfilePage = () => {
  const { t } = useTranslation();
  return (
    <>
      <SEOMeta titleSuffix={t('profile')} />
      <ElementCenter className="h-screen">
        <Form />
      </ElementCenter>
    </>
  );
};

ProfilePage.Layout = AuthLayout;

export const getServerSideProps = withSSAuth(withTranslations());

export default ProfilePage;
