import Button from '@base/components/button';
import Input from '@base/components/form/input';
import useSessionClient from '@base/hooks/session/useSessionClient';
import alert from '@base/utils/alert';
import { validatorResolver } from '@base/utils/validator';
import useLogin from 'hooks/auth/useLogin';
import useProfile from 'hooks/auth/useProfile';
import head from 'lodash/head';
import Link from 'next/link';
import { useRouter } from 'next/router';
import FormCard from 'pages/auth/components/FormCard';
import { AppState } from 'pages/store';
import React, { useEffect } from 'react';
import { useForm } from 'react-hook-form';
import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';

const Form = () => {
  const { t } = useTranslation();
  const router = useRouter();
  const { isLoaded, data } = useSessionClient();
  const { getProfile } = useProfile();
  const {
    register,
    handleSubmit,
    formState: { errors }
  } = useForm({
    resolver: validatorResolver
  });
  const { pending, login, result } = useLogin();
  const { loading } = useSelector((state: AppState) => state.nprogress);

  useEffect(() => {
    if (isLoaded && data) {
      getProfile();
      router.replace(String(router.query?.callbackUrl || '/'));
    }
  }, [isLoaded]);

  useEffect(() => {
    if (result?.error) {
      alert.error(result?.error);
    }
  }, [result]);

  const onSubmit = async (values) => {
    login(
      {
        email: values.email,
        password: values.password
      },
      'credentials'
    );
  };
  return (
    <FormCard title={t('form:login.title')}>
      <form onSubmit={handleSubmit(onSubmit)}>
        <div className="flex flex-col">
          <div>
            <Input
              label={t('form:login.label.email')}
              rules="required|email"
              error={head(errors.email)}
              {...register('email')}
            />
          </div>
          <div>
            <Input
              label={t('form:login.label.password')}
              rules="required"
              error={head(errors.password)}
              type="password"
              {...register('password')}
            />
          </div>
          <div className="text-right text-sm">
            <Link href="/auth/forgot-password">{t('form:login.button.forgotPassword')}</Link>
          </div>

          <div className="p-2 text-center">
            <Button className="primary" loading={pending} disabled={loading || pending}>
              {t('form:login.button.login')}
            </Button>
          </div>

          <div className="text-center text-sm">
            <span>
              {t('form:login.suggest.register')}
              <Link href="/auth/register">
                <a className="ml-2">{t('form:login.button.register')}</a>
              </Link>
            </span>
          </div>
        </div>
      </form>
    </FormCard>
  );
};

export default Form;
