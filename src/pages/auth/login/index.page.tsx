import ElementCenter from '@base/components/common/ElementCenter';
import SEOMeta from '@base/components/head/SEOMeta';
import { withTranslations } from 'middleware/withSSTranslations';
import { withSSUnAuth } from 'middleware/withSSUnAuth';
import { useTranslation } from 'next-i18next';
import AuthLayout from 'pages/auth/components/layout';
import Form from './components/Form';

const LoginPage = () => {
  const { t } = useTranslation();
  return (
    <>
      <SEOMeta titleSuffix={t('login')} />
      <ElementCenter className="h-screen">
        <Form />
      </ElementCenter>
    </>
  );
};

LoginPage.Layout = AuthLayout;

export const getServerSideProps = withSSUnAuth(withTranslations());

export default LoginPage;
