import useUI from '@base/hooks/ui/useUI';
import { withTranslations } from 'middleware/withSSTranslations';
import Master from 'pages/components/layout';
import React from 'react';

export default function ProductIndex() {
  const {
    ui: { screen }
  } = useUI();
  return <div>{JSON.stringify(screen)}</div>;
}

ProductIndex.Layout = Master;

export const getStaticProps = withTranslations();
