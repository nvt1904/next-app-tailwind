import ElementCenter from '@base/components/common/ElementCenter';
import Link from 'next/link';
import React from 'react';
import Logo from 'pages/components/logo';

function LogoHeader() {
  return (
    <Link href="/">
      <a href="">
        <ElementCenter>
          <Logo className="h-10 w-10 text-primary" />
        </ElementCenter>
      </a>
    </Link>
  );
}

export default LogoHeader;
