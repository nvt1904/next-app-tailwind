import classNames from 'classnames';
import React from 'react';
import LogoHeader from './LogoHeader';
import NavigationHeader from './NavigationHeader';

type Props = {
  fixed?: boolean;
};

export default function Header({ fixed }: Props) {
  return (
    <div
      style={{ height: 'var(--height-header)' }}
      className={classNames(`z-30 flex w-full justify-center bg-light shadow-lg dark:bg-dark`, {
        fixed: fixed
      })}
    >
      <div className="container flex justify-between align-middle">
        <LogoHeader />
        <NavigationHeader />
      </div>
    </div>
  );
}
