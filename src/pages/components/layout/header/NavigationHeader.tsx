import Link from 'next/link';
import React, { useState } from 'react';
import { XIcon, MenuIcon } from '@heroicons/react/outline';
import ElementCenter from '@base/components/common/ElementCenter';
import classNames from 'classnames';
import { useRouter } from 'next/router';
import { Menu, Transition } from '@headlessui/react';
import { useTranslation } from 'next-i18next';

export type Navigation = {
  href: string;
  title: string;
  blank?: boolean;
};

export default function NavigationHeader() {
  const router = useRouter();
  const { t } = useTranslation();
  const [navigators] = useState<Navigation[]>([
    {
      href: '/product',
      title: t('Product')
    },
    {
      href: '/message',
      title: t('message')
    },
    {
      href: '/auth/login',
      title: t('login')
    }
  ]);

  return (
    <ElementCenter>
      <>
        <nav className="relative hidden w-full justify-end md:flex ">
          <ul className="absolute top-full gap-3 rounded md:relative md:flex">
            {navigators.map((item) => (
              <li key={item.href}>
                <Link href={item.href}>
                  <a
                    className={classNames(
                      'rounded-full px-3 py-1.5 font-bold transition duration-200 ease-in-out hover:bg-primary hover:text-light',
                      {
                        'bg-primary text-light shadow-primary': router.pathname === item.href
                      }
                    )}
                  >
                    {item.title}
                  </a>
                </Link>
              </li>
            ))}
          </ul>
        </nav>
        <Menu>
          {({ open }) => (
            <>
              <div className="relative flex md:hidden ">
                <Menu.Button className="p-1 pr-0 text-primary">
                  {!open ? <MenuIcon className="h-6 w-6" /> : <XIcon className="h-6 w-6" />}
                </Menu.Button>
              </div>
              <Transition
                className="block-component fixed top-14 right-0 z-30 h-full w-full p-1"
                enter="transition ease-out duration-100"
                enterFrom="transform opacity-0 scale-95"
                enterTo="transform opacity-100 scale-100"
                leave="transition ease-in duration-100"
                leaveFrom="transform opacity-100 scale-100"
                leaveTo="transform opacity-0 scale-95"
              >
                <Menu.Items className="flex flex-col gap-1">
                  {navigators.map((item) => (
                    <Menu.Item key={item.href}>
                      <Link href={item.href}>
                        <a
                          className={classNames(
                            'w-full rounded-lg px-3 py-1.5 transition duration-200 ease-in-out hover:bg-primary hover:text-light',
                            {
                              'bg-primary text-light': router.pathname === item.href
                            }
                          )}
                        >
                          {item.title}
                        </a>
                      </Link>
                    </Menu.Item>
                  ))}
                </Menu.Items>
              </Transition>
            </>
          )}
        </Menu>
      </>
    </ElementCenter>
  );
}
