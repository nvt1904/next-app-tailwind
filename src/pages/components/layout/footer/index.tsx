import ElementCenter from '@base/components/common/ElementCenter';
import React from 'react';

function Footer() {
  return (
    <div className="container h-16">
      <ElementCenter>
        <span>
          create by @nvt1904 (
          <a
            className="text-primary"
            href="https://gitlab.com/nvt1904/next-app-tailwind"
            target="_blank"
            rel="noreferrer"
          >
            repo
          </a>
          )
        </span>
      </ElementCenter>
    </div>
  );
}

export default Footer;
