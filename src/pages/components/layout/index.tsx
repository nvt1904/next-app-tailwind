import classNames from 'classnames';
import { useRouter } from 'next/router';
import ElementCenter from '@base/components/common/ElementCenter';
import Loading from '@base/components/common/Loading';
import OGMeta from '@base/components/head/OGMeta';
import SEOMeta from '@base/components/head/SEOMeta';
import Footer from 'pages/components/layout/footer';
import Header from 'pages/components/layout/header';
import React, { useState } from 'react';
import PerfectScrollbar from 'react-perfect-scrollbar';

type Props = {
  children?: JSX.Element | React.ReactNode | React.ReactNodeArray;
};

function Master(props: Props) {
  const router = useRouter();
  const [fixedHeader, setFixedHeader] = useState(false);

  return (
    <div className="relative h-screen">
      <SEOMeta />
      <OGMeta />
      <PerfectScrollbar
        onScrollUp={() => !fixedHeader && setFixedHeader(true)}
        onScrollDown={() => fixedHeader && setFixedHeader(false)}
      >
        <Header fixed={fixedHeader} />

        <div
          className={classNames({
            'min-h-mh-content-page': !fixedHeader,
            'min-h-screen': fixedHeader
          })}
          style={{
            paddingTop: fixedHeader ? 'var(--height-header)' : 0
          }}
        >
          {router.isFallback ? (
            <ElementCenter>
              <Loading />
            </ElementCenter>
          ) : (
            props.children
          )}
        </div>
        <Footer />
      </PerfectScrollbar>
    </div>
  );
}

export default Master;
