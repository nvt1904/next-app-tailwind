import ImageCustom from '@base/components/image/ImageCustom';
import React, { CSSProperties } from 'react';

export type BannerProps = {
  banner: {
    src: string;
    title?: JSX.Element | string;
    description?: JSX.Element | string;
  };
  className?: string;
  style?: CSSProperties;
};

export default function Banner(props: BannerProps) {
  const { banner, className, style } = props;
  return (
    <div className={`relative ${className}`} style={style}>
      <ImageCustom priority={true} loading="eager" src={banner.src} alt={banner.src} />
      <div className="absolute top-1/3 w-full text-center text-light">
        <h1 className="mb-3 text-4xl font-bold text-primary">{banner.title}</h1>
        <p className="font-medium text-xl">{banner.description}</p>
      </div>
    </div>
  );
}
