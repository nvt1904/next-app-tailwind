import { ArrowNarrowRightIcon } from '@heroicons/react/outline';
import ImageCustom from '@base/components/image/ImageCustom';
import Link from 'next/link';
import React from 'react';

export default function List({ products, title }) {
  return (
    <>
      <div className="flex items-center justify-between">
        <h2 className="my-4 text-2xl font-bold">{title}</h2>
        <Link href="/product#">
          <a className="flex items-center gap-2">
            More {`  `}
            <ArrowNarrowRightIcon className="h-6 w-6" />
          </a>
        </Link>
      </div>
      <div className="grid grid-cols-2 gap-2 py-2 md:grid-cols-3 md:gap-4 lg:grid-cols-4">
        {products?.map((product) => (
          <div
            key={product.id}
            className="block-component relative cursor-pointer border-none hover:shadow-xl"
          >
            <div className="w-100 relative aspect-square overflow-hidden rounded-t-lg">
              <ImageCustom
                className="rounded-t-lg transition-all delay-100 duration-500 ease-out hover:scale-125"
                src={product.image}
                alt={product.image}
              />
            </div>
            <div className="p-2">
              <h2 className="font-bold line-clamp-1">{product.name}</h2>
              <p className="text-sm line-clamp-2">{product.description}</p>
            </div>
            <div className="absolute top-0 right-0 h-auto w-auto rounded-tr-lg bg-primary p-2 text-white">
              {`$ ${product.price}`}
            </div>
          </div>
        ))}
      </div>
    </>
  );
}
