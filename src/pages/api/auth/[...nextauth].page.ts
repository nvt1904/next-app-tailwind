import { login, refreshToken } from 'apis/auth';
import { addMinutes } from 'date-fns';
import pick from 'lodash/pick';
import { NextApiRequest, NextApiResponse } from 'next';
import NextAuth from 'next-auth';
import CredentialsProvider from 'next-auth/providers/credentials';
import { signOut } from 'next-auth/react';

type AuthCredentials = { email: string; password: string; locale?: string };

function handle(req: NextApiRequest, res: NextApiResponse) {
  return NextAuth(req, res, {
    providers: [
      CredentialsProvider({
        id: 'credentials',
        name: 'credentials',
        credentials: {
          email: { label: 'Email', type: 'text' },
          password: { label: 'Password', type: 'password' },
          locale: { label: 'Locale', type: 'text' }
        },
        async authorize(credentials: AuthCredentials) {
          return login(
            {
              email: credentials.email,
              password: credentials.password
            },
            { locale: credentials.locale }
          ).then((result) => {
            if (result.error) {
              throw new Error(String(result?.message));
            }
            return result;
          });
        }
      })
    ],
    jwt: {
      secret: process.env.JWT_SECRET
    },
    pages: {
      signIn: '/auth/login'
    },
    session: {
      strategy: 'jwt'
    },
    callbacks: {
      async jwt({ token, user }) {
        let newToken = token;
        if (!newToken.accessToken) {
          newToken = user;
        }
        if (newToken?.expires && new Date(newToken.expires) <= addMinutes(new Date(), 1)) {
          return refreshToken({
            refreshToken: String(newToken?.refreshToken || '')
          }).then((result) => {
            if (result.error) {
              return signOut();
            }
            return { ...newToken, ...result };
          });
        }
        return newToken;
      },
      async session({ session, token }) {
        session.token = pick(token, ['accessToken', 'refreshToken', 'expires']);
        delete session.user;
        return session;
      }
    }
  });
}

export default handle;
