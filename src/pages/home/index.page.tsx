import SEOMeta from '@base/components/head/SEOMeta';
import { withRevalidate } from 'middleware/withRevalidate';
import { withTranslations } from 'middleware/withSSTranslations';
import { useTranslation } from 'next-i18next';
import Master from 'pages/components/layout';
import React from 'react';
function IndexPage() {
  const { t } = useTranslation();
  return (
    <div>
      <SEOMeta titleSuffix={t('home')} />
      Hello World!
    </div>
  );
}

IndexPage.Layout = Master;

export const getStaticProps = withTranslations(
  [],
  withRevalidate(undefined, () => {
    return {
      props: {}
    };
  })
);

export default IndexPage;
