import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { merge } from 'rxjs';

export enum StatusProfile {
  DEFAULT = 'null',
  INIT = 'init',
  LOADING = 'loading',
  LOADED = 'loaded'
}

export enum Status {
  ACTIVE = 'active',
  INACTIVE = 'inactive'
}

export type DataProfile = {
  id: number;
  avatar?: number;
  name: string;
  email: string;
  status: Status;
};

export type ProfileState = {
  status: StatusProfile;
  data?: DataProfile | null;
};

const initialState: ProfileState = {
  status: StatusProfile.DEFAULT,
  data: null
};

const profile = createSlice({
  name: 'profile',
  initialState,
  reducers: {
    changeStatusProfile: (state, action: PayloadAction<StatusProfile>) => {
      return { ...state, status: action.payload };
    },
    changeDataProfile: (state, action: PayloadAction<DataProfile>) => {
      return { ...state, data: action.payload };
    }
  }
});

const { reducer, actions } = profile;
export const { changeStatusProfile, changeDataProfile } = actions;
export default reducer;
