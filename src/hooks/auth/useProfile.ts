import authApi from 'apis/auth';
import pick from 'lodash/pick';
import { AppState } from 'pages/store';
import { useDispatch, useSelector } from 'react-redux';
import { changeDataProfile, changeStatusProfile, DataProfile, StatusProfile } from './reducer';

function useProfile() {
  const data = useSelector((state: AppState) => state.data);
  const status = useSelector((state: AppState) => state.profile.status);
  const dispatch = useDispatch();

  const setData = (newData: DataProfile) => {
    dispatch(changeDataProfile(newData));
  };

  const setStatus = (newStatus: StatusProfile) => {
    dispatch(changeStatusProfile(newStatus));
  };

  const getProfile = async () => {
    setStatus(StatusProfile.LOADING);
    const res = await authApi.getProfile();
    if (!res.error) {
      const data = pick(res, ['id', 'avatar', 'name', 'email', 'status']);
      setData(data);
    } else {
      setData(null);
    }
    setStatus(StatusProfile.LOADED);
    return res;
  };

  return {
    status,
    getProfile,
    setData,
    setStatus,
    data,
    isInit: status !== StatusProfile.DEFAULT,
    isLoading: status === StatusProfile.LOADING,
    isLoaded: status === StatusProfile.LOADED
  };
}

export default useProfile;
