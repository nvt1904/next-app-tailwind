import { useState } from 'react';
import authApi, { RegisterData, RegisterRes } from 'apis/auth';

function useRegister() {
  const [pending, setPending] = useState(false);
  const [result, setResult] = useState<RegisterRes>();
  const register = async (data: RegisterData) => {
    setPending(true);
    const res = await authApi.register(data);
    setResult(res);
    setPending(false);
    return res;
  };

  return {
    pending,
    register,
    result
  };
}

export default useRegister;
