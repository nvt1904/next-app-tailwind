import useLocale from '@base/hooks/locale/useLocale';
import useSessionClient from '@base/hooks/session/useSessionClient';
import { signIn, SignInResponse } from 'next-auth/react';
import { useState } from 'react';

export type DataLoginNext = {
  email: string;
  password: string;
  redirect?: boolean;
  callbackUrl?: string;
};

function useLogin() {
  const { locale } = useLocale();
  const { getSession } = useSessionClient();
  const [pending, setPending] = useState(false);
  const [result, setResult] = useState<SignInResponse>();
  const login = async (data: DataLoginNext, type?: string) => {
    setPending(true);
    const res = await signIn(type, { redirect: false, ...data, locale: locale.key });
    setResult(res);
    setPending(false);
    getSession();
    return res;
  };

  return {
    pending,
    login,
    result
  };
}

export default useLogin;
