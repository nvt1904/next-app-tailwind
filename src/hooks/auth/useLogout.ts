import { signOut, SignOutParams } from 'next-auth/react';
import { useState } from 'react';

function useLogout() {
  const [pending, setPending] = useState(false);
  const logout = async (type?: SignOutParams<true>) => {
    setPending(true);
    signOut(type);
    setPending(false);
  };

  return {
    pending,
    logout
  };
}

export default useLogout;
