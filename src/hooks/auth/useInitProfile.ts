import useSessionClient from '@base/hooks/session/useSessionClient';
import { useEffect } from 'react';
import { StatusProfile } from './reducer';
import useProfile from './useProfile';

export default function useInitProfile() {
  const { isLoaded, isLogged } = useSessionClient();
  const { isInit, getProfile, setStatus } = useProfile();

  useEffect(() => {
    if (!isInit && isLoaded) {
      console.log('initProfile');
      setStatus(StatusProfile.INIT);
      if (isLogged) {
        getProfile();
      }
    }
  }, [isLoaded, isInit, isLogged]);
}
