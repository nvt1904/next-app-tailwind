import { useState } from 'react';
import authApi, { ForgotPasswordData, ForgotPasswordRes } from 'apis/auth';

function useForgotPassword() {
  const [pending, setPending] = useState(false);
  const [result, setResult] = useState<ForgotPasswordRes>();
  const forgotPassword = async (data: ForgotPasswordData) => {
    setPending(true);
    const res = await authApi.forgotPassword(data);
    setResult(res);
    setPending(false);
    return res;
  };

  return {
    pending,
    forgotPassword,
    result
  };
}

export default useForgotPassword;
