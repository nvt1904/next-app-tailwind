import { useState } from 'react';
import authApi, { UpdateProfileData, UploadProfileRes } from 'apis/auth';

function useUpdateProfile() {
  const [pending, setPending] = useState(false);
  const [result, setResult] = useState<UploadProfileRes>();
  const updateProfile = async (data: UpdateProfileData) => {
    setPending(true);
    const res = await authApi.updateProfile(data);
    setResult(res);
    setPending(false);
    return res;
  };

  return {
    pending,
    updateProfile,
    result
  };
}

export default useUpdateProfile;
