import { subject } from '@base';
import useSessionClient from '@base/hooks/session/useSessionClient';
import { pushMessages } from 'hooks/message/reducer';
import { useEffect, useRef } from 'react';
import { useDispatch } from 'react-redux';
import SocketIOClient, { Socket } from 'socket.io-client';

export const socketIOClient = (accessToken?: string) =>
  SocketIOClient(process.env.NEXT_PUBLIC_SERVER_SOCKET_IO || 'ws://localhost:4000', {
    transports: ['websocket'],
    query: { accessToken }
  });

function useInitSocketIO() {
  const { isLogged, data } = useSessionClient();
  const ioClient = useRef<Socket>();
  const dispatch = useDispatch();

  const onNewMessage = (newMessage) => {
    dispatch(pushMessages([newMessage]));
  };

  useEffect(() => {
    const disconnect = () => {
      if (ioClient.current) {
        ioClient.current.disconnect();
        console.log('disconnectSocket');
      }
    };
    const connect = async () => {
      disconnect();
      if (isLogged) {
        console.log('connectSocket');
        ioClient.current = socketIOClient(data?.accessToken);
        ioClient.current.on('info', (info) => console.log(info.message));
        ioClient.current.on('newMessage', onNewMessage);
      }
    };
    connect();
    return disconnect;
  }, [isLogged, data]);
}

export default useInitSocketIO;
