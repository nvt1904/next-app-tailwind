import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import cloneDeep from 'lodash/cloneDeep';
import { Conversation, ListConversationParams } from 'apis/conversation';
import { ListMessageParams, Message } from 'apis/message';

export type MessageState = {
  init: boolean;
  conversations: { [key: string]: Conversation };
  conversationId?: string;
  params: ListConversationParams;
  total: number;
  loading: boolean;
  messages: {
    [key: string]: {
      items: Message[];
      total: number;
      params: ListMessageParams;
      loading: boolean;
    };
  };
};

const initialState: MessageState = {
  init: false,
  conversations: {},
  params: { limit: 20, page: 1 },
  total: 0,
  loading: false,
  messages: {}
};

const message = createSlice({
  name: 'message',
  initialState,
  reducers: {
    changeInit: (state, action: PayloadAction<boolean>) => {
      state.init = action.payload;
    },
    changeConversationId: (state, action: PayloadAction<string>) => {
      state.conversationId = action.payload;
    },
    changeConversationParams: (state, action: PayloadAction<ListConversationParams>) => {
      state.params = action.payload;
    },
    changeConversationLoading: (state, action: PayloadAction<boolean>) => {
      state.loading = action.payload;
    },
    changeConversationTotal: (state, action: PayloadAction<number>) => {
      state.total = action.payload;
    },
    pushConversation: (state, action: PayloadAction<Conversation[]>) => {
      const newState = cloneDeep(state);
      const conversations = action.payload;
      conversations.forEach((conversation) => {
        newState.conversations[conversation.id] = conversation;
        if (!newState.messages[conversation.id]) {
          newState.messages[conversation.id] = {
            items: [],
            total: 0,
            params: { page: 1, limit: 20 },
            loading: false
          };
        }
      });
      Object.assign(state, newState);
    },
    changeMessageParams: (
      state,
      action: PayloadAction<{ conversationId: string; params: ListMessageParams }>
    ) => {
      const { conversationId, params } = action.payload;
      state.messages[conversationId].params = params;
    },
    changeMessageLoading: (
      state,
      action: PayloadAction<{ conversationId: string; loading: boolean }>
    ) => {
      const { conversationId, loading } = action.payload;
      state.messages[conversationId].loading = loading;
    },
    changeMessageTotal: (
      state,
      action: PayloadAction<{ conversationId: string; total: number }>
    ) => {
      const { conversationId, total } = action.payload;
      state.messages[conversationId].total = total;
    },
    pushMessages: (state, action: PayloadAction<Message[]>) => {
      const newState: MessageState = cloneDeep(state);
      const messages = action.payload;
      messages.forEach((message) => {
        const items = newState.messages[message.conversationId]?.items;
        if (items[0] && new Date(message.createdAt) < new Date(items[0].createdAt)) {
          items.unshift(message);
        } else {
          items.push(message);
          newState.conversations[message.conversationId].message = message;
        }
      });
      Object.assign(state, newState);
    }
  }
});

const { reducer, actions } = message;
export const {
  changeInit,
  changeConversationId,
  changeConversationTotal,
  changeConversationParams,
  changeConversationLoading,
  pushConversation,
  changeMessageParams,
  changeMessageLoading,
  changeMessageTotal,
  pushMessages
} = actions;
export default reducer;
