import { useState } from 'react';
import messageApi, { CreateMessageData, CreateMessageRes } from 'apis/message';

function useCreateMessage() {
  const [pending, setPending] = useState(false);
  const [result, setResult] = useState<CreateMessageRes>();

  const createMessage = async (data: CreateMessageData) => {
    setPending(true);
    const res = await messageApi.create(data);
    if (!res?.error) {
      setResult(res);
    } else {
      setResult(undefined);
    }
    setPending(false);
    return res;
  };
  return {
    pending,
    setPending,
    result,
    setResult,
    createMessage
  };
}

export default useCreateMessage;
