import messageApi, { ListMessageParams } from 'apis/message';
import cloneDeep from 'lodash/cloneDeep';
import merge from 'lodash/merge';
import { AppState } from 'pages/store';
import { useDispatch, useSelector } from 'react-redux';
import {
  changeMessageLoading,
  changeMessageParams,
  changeMessageTotal,
  pushMessages
} from './reducer';

function useListMessage() {
  const { messages } = useSelector((state: AppState) => state.message);
  const dispatch = useDispatch();

  const getListMessage = async (p: ListMessageParams) => {
    const conversationId = p.conversationId;
    const newParams = merge(cloneDeep(messages[conversationId].params), p, { conversationId });
    dispatch(changeMessageLoading({ conversationId, loading: true }));
    dispatch(changeMessageParams({ conversationId, params: newParams }));
    const res = await messageApi.list(newParams);
    if (!res?.error) {
      dispatch(pushMessages(res.items));
      dispatch(changeMessageTotal({ conversationId, total: res.total }));
    }
    dispatch(changeMessageLoading({ conversationId, loading: false }));
    return res;
  };

  const loadMoreMessage = (conversationId: string) => {
    const {
      params: { page, limit },
      total
    } = messages[conversationId];
    if (page * limit < total) {
      getListMessage({ conversationId, page: page + 1 });
    }
  };

  return {
    messages,
    getListMessage,
    loadMoreMessage
  };
}

export default useListMessage;
