import merge from 'lodash/merge';
import { AppState } from 'pages/store';
import { useDispatch, useSelector } from 'react-redux';
import conversationApi, { ListConversationParams } from 'apis/conversation';
import {
  changeConversationId,
  changeConversationLoading,
  changeConversationParams,
  changeConversationTotal,
  changeInit,
  pushConversation
} from './reducer';

function useListConversation() {
  const { init, conversations, conversationId, params, total, loading } = useSelector(
    (state: AppState) => state.message
  );
  const dispatch = useDispatch();
  const getListConversation = async (p: ListConversationParams = {}) => {
    dispatch(changeConversationLoading(true));
    const newParams = merge(params, p);
    dispatch(changeConversationParams(params));
    const res = await conversationApi.list(newParams);
    if (!res?.error) {
      dispatch(pushConversation(res.items));
      dispatch(changeConversationTotal(res.total));
    }
    dispatch(changeConversationLoading(false));
    dispatch(changeInit(true));
    return res;
  };

  const loadMoreConversation = () => {
    const { page, limit } = params;
    if (page * limit < total) {
      getListConversation({ page: page + 1 });
    }
  };

  const changeConversation = (id?: string) => {
    dispatch(changeConversationId(id));
  };

  return {
    init,
    loading,
    params,
    conversations,
    conversationId,
    getListConversation,
    loadMoreConversation,
    changeConversation
  };
}

export default useListConversation;
