import { useState } from 'react';
import cloudinaryApi, { UploadTempRes } from 'apis/cloudinary';

function useUploadCloudinary() {
  const [pending, setPending] = useState(false);
  const [result, setResult] = useState<UploadTempRes>();
  const uploadCloudinary = async (imageBase64: string) => {
    if (imageBase64) {
      setPending(true);
      const res = await cloudinaryApi.uploadTemp(imageBase64);
      setResult(res);
      setPending(false);
      return res;
    }
  };

  return {
    pending,
    uploadCloudinary,
    result
  };
}

export default useUploadCloudinary;
