import merge from 'lodash/merge';
import { GetServerSideProps, GetStaticPaths, GetStaticProps } from 'next';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';

const defaultNamespaces = ['common', 'messages', 'form', 'theme'];

export function withTranslations(
  namespaces: string[] = [],
  cb?: GetServerSideProps | GetStaticProps | GetStaticPaths
) {
  return async (context) => {
    return merge(cb ? await cb(context) : { props: {} }, {
      props: {
        ...(await serverSideTranslations(context.locale, [...defaultNamespaces, ...namespaces]))
      }
    });
  };
}
