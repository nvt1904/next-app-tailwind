import merge from 'lodash/merge';
import { GetStaticProps } from 'next';

export function withRevalidate(
  revalidate = Number(process.env.PAGE_PROPS_REVALIDATE || 1),
  cb?: GetStaticProps
) {
  return async (ctx) => {
    return merge(cb ? await cb(ctx) : {}, { revalidate });
  };
}
