import merge from 'lodash/merge';
import { GetServerSideProps } from 'next';
import { getSession } from 'next-auth/react';
import { stringify } from 'query-string';

// only getServerSideProps
export function withSSAuth(cb?: GetServerSideProps) {
  return async (ctx) => {
    const session = await getSession(ctx);
    if (!session) {
      return {
        redirect: {
          destination: `/auth/login?${stringify({ callbackUrl: ctx.resolvedUrl })}`,
          permanent: false
        }
      };
    }
    return merge(cb ? await cb(ctx) : {}, { props: { session } });
  };
}
