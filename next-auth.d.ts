import 'next-auth';
import 'next-auth/jwt';

export interface Token {
  accessToken: string;
  refreshToken: string;
  expires: string;
}

/** Example on how to extend the built-in types for JWT */
declare module 'next-auth/jwt' {
  interface JWT extends Auth {
    expires: string;
  }
}

/** Example on how to extend the built-in session types */
declare module 'next-auth' {
  interface Session {
    token?: Token;
  }

  // eslint-disable-next-line @typescript-eslint/no-empty-interface
  interface User extends Token {}
}
