module.exports = {
  mode: 'jit',
  content: ['./src/**/**/*.{jsx,tsx}'],
  darkMode: 'class',
  theme: {
    fontWeight: {
      light: 300,
      normal: 400,
      bold: 500
    },
    extend: {
      colors: {
        primary: 'var(--color-primary)',
        secondary: 'var(--color-secondary)',
        success: 'var(--color-success)',
        danger: 'var(--color-danger)',
        warning: 'var(--color-warning)',
        info: 'var(--color-info)',
        light: 'var(--color-light)',
        'light-secondary': 'var(--color-light-secondary)',
        dark: 'var(--color-dark)',
        'dark-secondary': 'var(--color-dark-secondary)',
        white: '#fff',
        black: '#000'
      },
      minHeight: {
        inherit: 'inherit',
        screen: 'var(--height-screen)',
        'mh-content-page': 'var(--mh-content-page)'
      },
      height: {
        inherit: 'inherit',
        screen: 'var(--height-screen)',
        'mh-content-page': 'var(--mh-content-page)'
      },
      maxHeight: {
        inherit: 'inherit',
        screen: 'var(--height-screen)',
        'mh-content-page': 'var(--mh-content-page)'
      },
      container: {
        center: true,
        padding: '.5rem'
      }
    }
  },
  variants: {
    extend: { display: ['dark'], typography: ['dark'] }
  },
  plugins: [require('@tailwindcss/typography'), require('@tailwindcss/line-clamp')]
};
